<?php

use Symfony\Component\Dotenv\Dotenv;

$dotenv = new Dotenv();
$path = __DIR__ . '/.env';

if (file_exists($path)) {
  $dotenv->loadEnv($path);
}
