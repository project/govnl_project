# GovNL Project

GovNL Drupal Composer Project

## Getting started

Create a new project using this composer template.

Change `PROJECT_NAME` to the name of your project.

```
composer create-project drupal/govnl_project PROJECT_NAME -s dev --no-install
```

## Installation

Run ```composer install```

Copy .env.example to .env and set the database configuration settings.

If you have a webserver running you should be able to access your site.

## Install with ddev
Change the "name" setting in .ddev/config.yaml to your projects name.

Run:

```ddev composer install```

```ddev start```

```ddev launch```

For more information about ddev see https://ddev.readthedocs.io/en/stable/

## Information

See [GovNL](drupal.org/project/govnl) for more information.
